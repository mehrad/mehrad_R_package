# mehrad R package

This is Mehrad's personal R package. It contains some of the QoL (quality of life) functions that i wrote to make my life easier adn my workflow faster.

Enjoy!


## Install

Install from the git repository:

```r
if (!require("devtools")) install.packages("devtools")
devtools::install_git(url = "https://codeberg.org/mehrad/mehrad_R_package/")
```

Alternatively you can built it yourself. In your terminal type:

```sh
git clone https://codeberg.org/mehrad/mehrad_R_package.git
cd mehrad_R_package
make build
make install
```

## Contribution

To contribute, please make sure NOT to push `man/*` as all the documkentations should be done in the `.R` files in Roxygen format.

## License

The code is free to use but I would like to think that I have created some something cool and I would like to be acknowledged. So I used Artistic License 2.0 for this repo. Any ideas/suggestion/criticism is welcome.
